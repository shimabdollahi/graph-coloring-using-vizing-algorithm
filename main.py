# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 12:26:37 2021

@author: Shima Abdollahi
"""
from random import randint
import networkx as nx
###########################################################
#Global variables:
NODES=[]
EDGES=[]
V=0
E=0
RANDOMS=[]
MAX_DEGREE=0
############################################################
def init_RANDOMS(): 
    global RANDOMS
    for i in range(MAX_DEGREE+1):
        RANDOMS.append(randint(0, 0xFFFFFF))
    return
def produce_colors(): 
    colors = []
    for i in range(MAX_DEGREE+1):
        colors.append('#%06X' % RANDOMS[i])
    return colors

############################################################
def is_in(element,listt): 
    for x in listt:
        if(x==element):
            return True
    return False
#############################################################
"""simple graph representation

"""

class Node: 
    def __init__(self,free_colors,neighbor_nodes,label): #checked:)
        self.free_colors=free_colors
        self.neighbor_nodes=neighbor_nodes
        self.label=label
    def remove_free_color(self,color): #checked:)
        """remove color from free_colors list"""
        for i in range(len(self.free_colors)):
            if(color==self.free_colors[i]):
                del self.free_colors[i]
                break
    def add_free_color(self,color): #checked:)
        """add color to free_colors list"""
        self.free_colors.append(color)
        self.free_colors=list(set(self.free_colors))
    def get_a_free_color(self): #checked:)
        if(len(self.free_colors)>0):
            return self.free_colors[0]
        return None
    def is_free_for_color(self,color): #checked:)
        if(color=='#000000'):
           return True
        for c in self.free_colors:
            if(c==color):
                return True
        return False
    def add_neighbors(self,nodes): #nodes is a list of nodes(node) #checked:)
        for node in nodes:
            self.neighbor_nodes.append(node)#=list(set(self.neighbor_nodes+nodes))
    def set_free_colors(self,colors):
        self.free_colors=colors
    def degree(self):
        return len(self.neighbor_nodes)
class Edge: 
    def __init__(self,incident_node1,incident_node2): #checked:)
        self.colored=False
        self.color='#000000'
        self.incident_node1=incident_node1
        self.incident_node2=incident_node2
    def color_(self,color): #checked :)
        if(not self.colored):
            self.color=color
            self.colored=True
            self.incident_node1.remove_free_color(color)
            self.incident_node2.remove_free_color(color)
    def uncolor(self): #checked :)
        if(self.colored):
            free_color=self.color
            self.color='#000000';
            self.colored=False
            self.incident_node1.add_free_color(free_color)
            self.incident_node2.add_free_color(free_color)
    def change_color(self,already_color,new_color): #ckecked :)
        self.color=new_color
        self.incident_node1.remove_free_color(new_color)
        self.incident_node2.remove_free_color(new_color)
        self.incident_node1.add_free_color(already_color)
        self.incident_node2.add_free_color(already_color)
    def equals(self,v1,v2): #checked:)
        if(self.incident_node1.label==v1.label and self.incident_node2.label==v2.label):
            return True
        if(self.incident_node1.label==v2.label and self.incident_node2.label==v1.label):
            return True
        return False
    def print_edge(self):
        print(self.incident_node1.label+"\t"+self.incident_node2.label+"\t"+str(self.colored)+"\t\t"+self.color )
    def output_edge(self):
        print(self.incident_node1.label+"\t"+self.incident_node2.label+"\t"+self.color )
        
######################################################
def set_delta():
    global MAX_DEGREE
    for v in NODES:
        if(v.degree()>MAX_DEGREE):
            MAX_DEGREE=v.degree()
##########################################################################
#GRAPHICS:
def construct_visual_graph():
    G=nx.Graph()
    for e in EDGES:
        G.add_edge(e.incident_node1.label,e.incident_node2.label,color=e.color,weight=5)
    return G
def draw_graph(G):
    pos = nx.circular_layout(G)
    edges = G.edges()
    colors = [G[u][v]['color'] for u,v in edges]
    weights = [G[u][v]['weight'] for u,v in edges]
    nx.draw(G, pos, edges=edges, edge_color=colors, width=weights,with_labels=True,
        node_color='#E6E6FA',node_size=600)
    return
def paint_G():
    G=construct_visual_graph()
    draw_graph(G)
    return
def print_graph():
    print("v1:\tv2:\tcolored:\tcolor:")
    for e in EDGES:
        e.print_edge()
    return
def output():
    print(str(MAX_DEGREE)+"\t"+str(MAX_DEGREE+1))
    for e in EDGES:
        e.output_edge()
    return
############################################################
def get_edge(v1,v2): 
    for e in EDGES:
        if(e.equals(v1,v2)):       
            return e
    return None
def update_edge(e,updated_e):
    global EDGES
    for i in range(len(EDGES)):
        if(EDGES[i]==e):
            EDGES[i]=updated_e
            return
    return
################################################
def make_EDGES():
    global NODES
    global EDGES
    global E,V
    v_e=input().split()
    V=int(v_e[0])
    E=int(v_e[1])

    
    for i in range(V):
        NODES+=[Node([],[],str(i))]
    for i in range(E):
        line=input().split()
        v1=int(line[0])
        v2=int(line[1])
        NODES[v1].add_neighbors([NODES[v2]])
        NODES[v2].add_neighbors([NODES[v1]])
        EDGES+=[Edge(NODES[v1],NODES[v2])]
    set_delta()
    init_RANDOMS()
    for i in range(V):
        NODES[i].set_free_colors(produce_colors())
    return
def coloring_graph():
    for X in NODES:
        for f in X.neighbor_nodes:
            if(not get_edge(X,f).colored):
                color_edge(X,f)
    paint_G()
    output()
    #print_graph()
    return            
###################################################################333333
def main():
    make_EDGES()
    coloring_graph()
    return        

###########################################################      
def B(X,fan): 
    for v in X.neighbor_nodes:
        if(not(is_in(v,fan)) ):
            Xv=get_edge(X,v)
            if(Xv!=None):
                l=fan[len(fan)-1]
                if(l.is_free_for_color(Xv.color)):
                    return v
    return None
def get_maximal_fan(X,f): #X is a Node & Xf is an uncolored Edge so f is a neghbor of X #checked?
    fan=[f]
    v=B(X,fan)
    while(v!=None):
        fan+=[v]
        v=B(X,fan)
    return fan
        
def invert_color(color,c,d):
    if(color==c):
        return d;
    return c;
def maximal_cd_path(X,c,d):
    path=[]
    color=d
    node=X
    reach_end=False
    while(not reach_end):
        reach_end=True
        for n in node.neighbor_nodes:
            e=get_edge(n,node)
            if(e!=None and e.color == color):
                path+=[n]
                color=invert_color(color,c,d)
                node=n
                reach_end=False
    return path
"""invert cd path"""
def invert_cd_path(path,c,d): #path is represented by an array of nodes
    for i in range(len(path)-1):
        e=get_edge(path[i],path[i+1])
        new_color=invert_color(e.color,c,d)
        e.change_color(e.color,new_color)
    return
############################################################################
"""rotate fan function"""
def rotate(X,fan):
    for i in range(len(fan)-1):
        e1=get_edge(X,fan[i])
        e2=get_edge(X,fan[i+1])
        if(e2.colored):
            e1.color_(e2.color)
            e2.uncolor()
    return
##########################################################################
def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3
##########################################################################
"""coloring function"""  
def color_edge(X,f): #X: a Node , f: a neighbor Node so Xf is uncolored.
    maximal_fan=get_maximal_fan(X,f);
    l=maximal_fan[len(maximal_fan)-1]
    X_frees=X.free_colors
    f_frees=f.free_colors
    color_options=intersection(X_frees,f_frees)
    if(len(color_options)>0):
        e=get_edge(X,f)
        e.color_(color_options[0])
        return
    #else:
    c=X.get_a_free_color()
    d=l.get_a_free_color()
    cd_path=maximal_cd_path(X,c,d)
    invert_cd_path(cd_path,c,d)
    #initiating:
    w=l
    sub_fan=maximal_fan
    #finding w:
    for i in range(len(maximal_fan)):
        if(maximal_fan[i].is_free_for_color(d)):
            w=maximal_fan[i]
            sub_fan=maximal_fan[0:i+1]
    rotate(X,sub_fan)
    #get edge Xw the color d:
    get_edge(X,w).color_(d)
    #paint_G()
    return
main()            
          
   
    