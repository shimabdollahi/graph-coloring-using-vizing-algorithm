# Graph Coloring using Vizing Algorithm

This code colors every " finit graph with no self loops and no multiple edges " in way such that there is no two edges incident on a vertex that have the same color.
I have used the Vizing 's theorem proofed by J. Misra & David Gries


# Guide to use program:
    INPUT FORM:
        v(G)    e(G) (first line consist of number of vertex of graph +space+ number of edges of graph)
        v1      v2   (other lines: each line consist of number of vertices incident on each edge)
        .
        .
        .

    INPUT EXAMPLE:
        5   4
        1   2
        1   3
        1   4
        2   3

    OUTPUT FORM:
        A visual colored graph plus color codes assigned to each edge
